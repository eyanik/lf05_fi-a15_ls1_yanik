﻿import java.util.Scanner;
public class Fahrkartenautomat {
	static Scanner tastatur = new Scanner(System.in);
	//TODO: Verzweigung anzahlTickets > 10 UND/ODER anzahlTickets <= 0
	// --> Meldung; Anzahl wurde auf 1 gesetzt
	//		anzahlTicket = 1 

	//TODO: ticketPreis ist nicht negativ
	
	
	//Teil1
	public static void main(String[] args) {
		
		while(true) {
	       
	       double zuZahlenderBetrag = fahrkartenbestellungErfassen(), rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	       
	       fahrkartenAusgeben();
	       rueckgeldAusgeben(rückgabebetrag);
	       
	       
	       System.out.println("\nVergessen Sie nicht, den Fahrschein \n" +
	                "vor Fahrtantritt entwerten zu lassen!\n" +
	                "Wir wünschen Ihnen eine gute Fahrt. \n");
		}
		}

	
	//Teil2
	public static double fahrkartenbestellungErfassen() {
			double zuZahlenderBetrag;
	        int anzahlTickets;
	        
	        System.out.print("Willkommen zum Fahrkartenautomaten!\n");
	        System.out.print("Zu zahlender Betrag (EURO): ");
		    zuZahlenderBetrag = tastatur.nextDouble();
		    if (zuZahlenderBetrag <= 0) {
		    System.out.print("kein Gültiger Betrag, der Betrag wurde auf 1€ gesetzt\"\n");
		    zuZahlenderBetrag = 1;
		    }
		    else {
	            System.out.println("Gültiger Betrag, der Betrag wurde auf " + zuZahlenderBetrag + "€ gesetzt" );
	           
		    	
		    } 
		    
		    
	        System.out.print("Ticket Anzahl: ");
	        
	        // Zuweisung des Werts
	        anzahlTickets = tastatur.nextInt();
	        
	        if (anzahlTickets <= 10 & anzahlTickets >= 1) {
	        System.out.print("Gültige Menge an Tickets\n");
	        
	        }
	        else {
	            System.out.println("Keine Gültige Anzahl an Tickets, die Menge der Tickets wurde auf 1 gesetzt.");
	            anzahlTickets = 1;
	        }
	       
	        
	        //Endbetrag
	        return zuZahlenderBetrag *anzahlTickets;
			}
	
			
	//Teil3
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
	        //Bezahlung
	 	    double eingezahlterGesamtbetrag = 0.0;
		    while(eingezahlterGesamtbetrag < zuZahlenderBetrag){
		    	
		    	   System.out.printf("%s%.2f%s" , "Noch zu zahlen: " , (zuZahlenderBetrag - eingezahlterGesamtbetrag) , (" Euro "));
		           System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
		           eingezahlterGesamtbetrag += tastatur.nextDouble();
		           
		       }
		       return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	        }

	
	//Teil4
	public static void fahrkartenAusgeben() {
	       // Fahrscheinausgabe
	       // -----------------
		   System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++){
	    	   
	          System.out.print("»»»»»");
	          try {
				Thread.sleep(350);
				
			} catch (InterruptedException e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }	       
		   System.out.println("\n\n");
		}

	
	//Teil5
	public static void rueckgeldAusgeben(double rückgabebetrag) {
	       // Rückgeldberechnung und -Ausgabe
	       // -------------------------------
		
	       if(rückgabebetrag > 0.0){
	    	   System.out.printf("%s%.2f%s" , "Der Rückgabebetrag in Höhe von " , rückgabebetrag , " Euro ");
	    	   System.out.printf("wird in folgenden Münzen ausgezahlt:\n");

	           while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.00;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.00;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }

	       
	       
	    
	//Aufgabe 4.5 pls add 4.3 and 4.4
	// Laut Aussage von Frau Krebs wurden die Aufgaben 2 und 3 gelöst.
	       

	}

}