import java.util.Scanner;

public class Aufgabe4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Wie ist der Betrag?");

        Scanner in = new Scanner(System.in);
        double nochZuZahlen = in.nextDouble();

        if(nochZuZahlen > 0 && nochZuZahlen < 100) {
            nochZuZahlen = nochZuZahlen - (nochZuZahlen * 0.10);
            System.out.println("Sie haben 10% Rabatt bekommen!");
            System.out.println("Der Betrag ist " + nochZuZahlen + " �");
        }
        else if(nochZuZahlen > 100 && nochZuZahlen < 500) {
            nochZuZahlen = nochZuZahlen - (nochZuZahlen * 0.15);
            System.out.println("Sie haben 15% Rabatt bekommen!");
            System.out.println("Der Betrag ist " + nochZuZahlen + " �");
        }
        else if(nochZuZahlen > 500) {
            nochZuZahlen = nochZuZahlen - (nochZuZahlen * 0.20);
            System.out.println("Sie haben 20% Rabatt bekommen!");
            System.out.println("Der Betrag ist " + nochZuZahlen + " �");
        }
	}

}
